<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use App\Win;
use App\Ward;
class PersonController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ('person.index') {
            $search = $request->get('search');
            $persons = Person::select('name','phone','gender','id');
            if (isset($search) && $search != null) {
                $persons = $persons->where('name', 'LIKE', "%{$search}%")
                    ->orwhere('phone', 'LIKE', "%{$search}%")
                    ->orWhere('gender', 'LIKE', "{$search}%");
            }
            $persons = $persons->latest()->paginate(100);
            $alldata = Person::all();
            $count = count($alldata);
            return view('person.index', compact('persons','count'));
        } else {// true
            return redirect('/');
        }
    }        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $wards = Ward::orderBy('title','asc')->pluck('title', 'id');
            return view('person.create',compact('wards'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
//            $ward = explode(",",$ward->ward_name);

            $persons = new Person;
            $persons->unique_id = $request->unique_id;
            $persons->name = $request->name;
            $persons->gender = $request->gender;
            $persons->father_name = $request->father_name;
            $persons->mother_name = $request->mother_name;
            $persons->address = $request->address;
            $persons->address1 = $request->address1;
            $persons->address2 = $request->address2;
            $persons->category = $request->category;
            $persons->ref = $request->ref;
            $persons->area = $request->area;
            $persons->ward_id = $request->ward_id;
            $persons->phone = $request->phone;
            $persons->email = $request->email;
            $persons->nid = $request->nid;


            if ( $persons->save()) {
                return redirect()->back()->with('message_success', "Insert Successfully");
            } else {
                return redirect()->back()->with('message_error', "Sorry Please try again.");
            }
           // return view('person.index',compact('win','persons'));
         // $persons->ward()->sync($request->ward);
        }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $person= Person::find($id);
        return view('person.show')->with('person',$person);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wards = Ward::orderBy('title','asc')->pluck('title', 'ward_name');
        $person = Person::where('id',$id)->first();
        return view('person.edit',compact('person','wards'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $persons = Person::find($id);
        // $lotteries->unique_id= $request->unique_id;
        $persons->name= $request->name;
        $persons->gender= $request->gender;
        $persons->father_name= $request->father_name;
        $persons->mother_name= $request->mother_name;
        $persons->address= $request->address;
        $persons->address1= $request->address1;
        $persons->address2= $request->address2;
        $persons->category = $request->category;
        $persons->ref = $request->ref;
        $persons->area= $request->area;
        $persons->ward_name = $request->ward_name;
        $persons->phone= $request->phone;
        $persons->email = $request->email;
        $persons->nid= $request->nid;
        $persons->is_win = $request->is_win;//this field will be 0 for every insert...no need any in field in form
//        $persons->date= $request->date;// no need to store this field remove it from form default will be null..these tow fields
                                        //will update only when win the lotery..

        $persons->save() ;
      //  $persons->ward()->sync($request->ward_name);
        return redirect(route('person.index'))->withMessage(' Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Person::destroy($id);
        return redirect()->back()->withMessage('Deleted Successfully');
    }
}
