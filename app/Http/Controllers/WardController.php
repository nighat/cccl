<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ward;
class WardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $wards = Ward::latest()->paginate(5);
        return view('ward.index',compact('wards'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ward.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $ward = new Ward;
        $ward->title = $request->title;
        $ward->ward_name = $request->ward_name;
        $ward->save();


        return redirect(route('ward.index'))->withMessage('ward Added');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ward = Ward::find($id);
        return view('ward.show')->with('ward',$ward);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ward = Ward::where('id',$id)->first();
        return view('ward.edit',compact('ward'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $ward = Ward::find($id);
       $ward->title = $request->title;
      $ward->ward_name= $request->ward_name;
        $ward->percent= $request->percent;
        $ward->save();

        return redirect(route('ward.index'))->withMessage('Wards Is Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ward::where('id',$id)->delete();
        return redirect()->back()->withMessage('Ward Is Deleted');
    }


    public function totalWinner()
    {
        $wards = Ward::all();
        return view('ward.totalWinner',compact('wards'));

    }

    public function saveMaxWinner(Request $r)
    {
        dd($r);

    }
}


