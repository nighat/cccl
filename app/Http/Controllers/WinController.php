<?php

namespace App\Http\Controllers;

use App\Mail\Winner;
use App\Ward;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Win;
use App\Person;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\QueryException as Exception;


class WinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

//        if ('win.index') {
//            $search = $request->get('search');
//            $wins = Win::select('title');
//            if (isset($search) && $search != null) {// you should refactor this code
//                $wins = $wins->where('title', 'LIKE', "%{$search}%");
//            }
//            $wins = $wins->latest()->paginate(20);
//            $alldata = Win::all();
//            $count = count($alldata);
//            return view('win.index', compact('wins','count'));
//        } else {// true
//            return redirect('/');
//        }
        $wins = Win::latest()->paginate(10);
        return view('win.index',compact('wins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $win_ids = Person::where('is_win',0)->pluck('id');
        $wards = Ward::orderBy('ward_name','asc')->pluck('ward_name','id');
        return view('win.create', compact('win_ids','wards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $i = 0;
        $wins = [];
       if($request->differ_in_gender == 'yes'){
           foreach ($request->ward_ids as  $ward_id){

               $ward = Ward::findOrFail($ward_id);

               $numberOfMail = Person::IsNotWinner()
                   ->where('gender','male')
                   ->where('ward_id', $ward_id)
                   ->pluck('id')
                   ->count();

               $numberOfFemale = Person::IsNotWinner()
                   ->where('gender','female')
                   ->where('ward_id', $ward_id)
                   ->pluck('id')
                   ->count();

               if($numberOfMail < $request->winners_per_word[$i]/2){
                   return redirect()->back()->withInput()->withErrors('There are '.$numberOfMail.' male persons available at '.$ward->ward_name. ' ward. Please check input for the ward..');
               }

               if($numberOfFemale < $request->winners_per_word[$i]/2){
                   return redirect()->back()->withInput()->withErrors('There are '.$numberOfFemale.' female persons available at '.$ward->ward_name. ' ward. Please check input for the ward..');
               }

               $male = Person::IsNotWinner()
                            ->where('gender','male')
                            ->where('ward_id', $ward_id)
                            ->pluck('id')
                            ->random($request->winners_per_word[$i]/2)
                            ->toArray();
               $female = Person::IsNotWinner()
                            ->where('gender','female')
                           ->where('ward_id', $ward_id)
                           ->pluck('id')
                           ->random($request->winners_per_word[$i]/2)
                           ->toArray();

               $winners = array_merge($male, $female);
               $winIds = array_merge($winners, $wins);
               $wins = $winIds;

               $i++;
           }
        }else{
           foreach ($request->ward_ids as  $ward_id){

               $ward = Ward::findOrFail($ward_id);

               $numberOfPerson = Person::IsNotWinner()
                   ->where('ward_id', $ward_id)
                   ->pluck('id')
                   ->count();

               if($numberOfPerson < $request->winners_per_word[$i]/2){
                   return redirect()->back()->withInput()->withErrors('There are '.$numberOfPerson.' male persons available at '.$ward->ward_name. ' ward. Please check input for the ward..');
               }

               $winners = Person::IsNotWinner()
                                ->where('ward_id', $ward_id)
                                ->pluck('id')
                                ->random($request->winners_per_word[$i])->toArray();
               $winIds = array_merge($winners, $wins);
               $wins = $winIds;
               $i++;
           }
        }

        foreach ($winIds as $id){
            $person = Person::findOrFail($id);
            $person->update(['is_win' => 1, 'win_date' => Carbon::now()->toDateTimeString()]);
        }

        $wins = new Win();
        $wins->title = $request->title;
        $wins->number_of_winners = array_sum($request->winners_per_word);
        $wins->win_ids = json_encode($winIds);
        //$wins->win_ids= $request->win_ids;
        $wins->save() ;

        return redirect(route('win.index'))->withMessage('Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Win $win)
    {
        $winners = json_decode($win->win_ids);
        //$winners = explode(",",$win->win_ids);
        $persons = Person::whereIn('id',$winners)->get();
        //$wards = Ward::whereIn('id','ward_name')->get();
        return view('win.show', compact('persons', 'win'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $win= Win::where('id',$id)->first();
        return view('win.edit',compact('win'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $wins = Win::find($id);
        $wins->title = $request->title;
        $wins->win_ids= $request->win_ids;
        $wins->save() ;

        return redirect(route('win.index'))->withMessage('Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Win::where('id',$id)->delete();
        return redirect()->back()->withMessage(' Deleted Successfully');
    }

    public function download($win_ids)
    {
        $win = Win::find($win_ids);
        $winners = explode(",",$win->win_ids);
        $persons = Person::whereIn('id',$winners)->get();
        $pdf = PDF::loadView('win.download',compact('persons'));
        return $pdf->download('win.pdf');
    }

    public function downloadExcel(Win $win)
    {
        $winners = json_decode($win->win_ids);
        $winners = Person::whereIn('id',$winners)->get(['name', 'gender', 'phone', 'email', 'address', 'ward_name']);
        return Excel::create('winners.',function($excel) use ($winners){
            $excel->sheet('mySheet',function($sheet) use ($winners){
                $sheet->fromArray($winners);
            });
        })->download("xlsx");
    }

    public function sendEmailToWinners(Win $win)
    {
        $winners = json_decode($win->win_ids);
        Person::whereIn('id',$winners)->get()->each(function($winner) use ($winners){
            try{
                \Mail::to($winner)->send(new Winner($winner));
            }catch (Exception $e){
                \Log::info($e->getMessage());
            }
        });
        return redirect()->back()->withMessage('Emails has been sent to the winners');

    }
}
