<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'persons';
    protected $fillable = ['win_ids', 'is_win', 'win_date'];
    protected $dates = ['win_date'];

    public function ward()
    {
        return $this->belongsTo(Ward::class);
    }

    public function scopeIsNotWinner($query){
        return $query->where('is_win', 0);
    }

}
