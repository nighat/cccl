<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Person::class, function (Faker $faker) {
    return [
        'unique_id'       => $faker->randomDigit,
        'name'       => $faker->name,
        'gender' => $faker->randomElement(['male', 'female']),
        'father_name'       => $faker->word,
        'mother_name'       => $faker->word,
        'address' => $faker->text,
        'address1' => $faker->word,
        'address2' => $faker->word,
        'category' => $faker->word,
        'ref' => $faker->word,
        'area' => $faker->word,
        'ward_id' => $faker->randomElement(App\Ward::pluck('id')->toArray()),
        'phone' => $faker->phoneNumber,
        'email' => $faker->word,
        'nid' => $faker->randomDigit,
        'win_date' => $faker->date,
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];
});
