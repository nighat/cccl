<?php

use Faker\Generator as Faker;

$factory->define(App\Ward::class, function (Faker $faker) {
    return [
        'title'       => $faker->name,
        'ward_name'       => $faker->word,
    ];
});
