<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class WardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wards = [
            [
                'title' => 'Pahartoil',
                'ward_name' => 'Saripara',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'title' => 'Agrabad',
                'ward_name' => 'T & T colloni',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'title' => 'Daunhat',
                'ward_name' => 'Mitiripara',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
        ];
        DB::table('wards')->insert($wards);
//        factory(App\Ward::class, 10)->create();
//
    }
}
