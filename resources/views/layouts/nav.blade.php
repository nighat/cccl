<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            {{--<a class="navbar-brand" href="{{ url('/') }}">--}}
            {{--{{ config('app.name', 'Laravel') }}--}}
            {{--</a>--}}
            {{--<a href="{{ route('home') }}">--}}
            {{--<img src="{{ asset('images/icc.png') }}" alt="CCCL">--}}
            {{--</a>--}}
            <ul class="nav navbar-nav navbar-reft">
                @auth
                <li><a href="{{ route('person.index') }}" class="bg-{{ (request()->segment(1) == 'person' ? 'danger' : 'info') }}"><b>Person</b></a></li>
                <li><a href="{{ route('win.index') }}"  class="bg-{{ (request()->segment(1) == 'win' ? 'danger' : 'info') }}"><b>Win</b></a></li>
                <li><a href="{{ route('ward.index') }}"  class="bg-{{ (request()->segment(1) == 'ward' ? 'danger' : 'info') }}"><b>Ward</b></a></li>
                {{--                    <li><a href="{{ route('ward.create') }}"><b>Random Selection</b></a></li>--}}
                @endauth
            </ul>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest
                <li><a href="{{ route('login') }}"><b>Login</b></a></li>
                <li><a href="{{ route('register') }}"><b>Register</b></a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endguest
            </ul>
        </div>
    </div>
</nav>