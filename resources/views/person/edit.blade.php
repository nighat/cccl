
@extends('layouts.master')

@section('content')

                <div class="panel panel-success">
                <div class="panel-heading"><h2 >Person Edit Form</h2></div>

                <div class="panel-body " >
                    <form class="form-horizontal" method="POST" action="{{ route('person.update',$person->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        {{--<div class="form-group">--}}
                            {{--<label for="unique_id" class="col-md-4 control-label">unique_id</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="unique_id" type="hidden" class="form-control colorize-theme6-bg" name="unique_id" value="{{ rand(1000,11000) }}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <label for="name" class="col-md-2 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control colorize-theme6-bg" name="name" value="{{ $person->name }}" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="father_name" class="col-md-2 control-label">Father_name</label>

                            <div class="col-md-6">
                                <input id="father_name" type="text" class="form-control colorize-theme6-bg" name="father_name" value="{{$person->father_name }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mother_name" class="col-md-2 control-label">Mother_name</label>

                            <div class="col-md-6">
                                <input id="mother_name" type="text" class="form-control colorize-theme6-bg" name="mother_name" value="{{ $person->mother_name }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-md-2 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control colorize-theme6-bg" name="address" value="{{ $person->address}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address1" class="col-md-2 control-label">Address 1</label>

                            <div class="col-md-6">
                                <input id="address1" type="text" class="form-control colorize-theme6-bg" name="address1" value="{{ $person->address1}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address2" class="col-md-2 control-label">Address 2</label>

                            <div class="col-md-6">
                                <input id="address2" type="text" class="form-control colorize-theme6-bg" name="address2"value="{{ $person->address2}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="area" class="col-md-2 control-label">Area</label>

                            <div class="col-md-6">
                                <input id="area" type="text" class="form-control colorize-theme6-bg" name="area" value="{{ $person->area}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Ward</label>
                            <div class="col-md-6">
                                <select class="form-control" name="ward_name" >
                                    <option >Select Ward</option>
                                    @foreach($wards as $key=>$value)
                                        <option {{ $person->ward_name==$key? 'selected':''}} value="{{ $key }}">


                                            {{ $value}}
                                        </option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-md-2 control-label">Phone</label>

                            <div class="col-md-6">
                                <input id="phone" type="number" class="form-control colorize-theme6-bg" name="phone" value="{{ $person->phone}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nid" class="col-md-2 control-label">NID</label>

                            <div class="col-md-6">
                                <input id="nid" type="number" class="form-control colorize-theme6-bg" name="nid" value="{{ $person->nid}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="is_win" class="col-md-2 control-label">Win</label>

                            <div class="col-md-6">
                                <input id="is_win" type="text" class="form-control colorize-theme6-bg" name="is_win"  value="{{ $person->is_win}}" >
                            </div>
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<label for="is_date" class="col-md-2 control-label">Date</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="is_date" type="date" class="form-control colorize-theme6-bg" name="is_date"  value="{{ $person->date}}" >--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <label for="gender" class="col-md-2 control-label">Gender:</label>


                            <div class="form-group col-md-6">
                                <div class="form-check" >
                                    <input class="form-check-input" type="radio" name="gender" id="male" value="male"  value="{{ $person->gender}}" checked>
                                    <label class="form-check-label" for="male">
                                        <p>Male</p>
                                    </label>
                                </div>
                                <div class="form-check ">
                                    <input class="form-check-input" type="radio" name="gender" id="female" value="female" value="{{ $person->gender}}">
                                    <label class="form-check-label" for="female">
                                        <p>Female</p>
                                    </label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">

                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{ route('person.index') }}" class=" btn btn-primary">Person</a>
                                <a href="{{ route('person.create') }}" class=" btn btn-primary"> <span class=" glyphicon glyphicon-plus-sign"></span>Create</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


@endsection
