@extends('layouts.master')

@section('content')

                <div class="panel panel-success">
    <div class="panel-heading">
        <h2 >
            Persons List
            <a href="{{ route('person.create') }}" class=" btn btn-primary pull-right"> <span class=" glyphicon glyphicon-plus-sign"></span>Add  Person </a>

        </h2>
        <form class="form-inline" action="{{ route('person.index') }}" method="get">
            <input type="search" class="form-control colorize-theme6-bg" id="search"
                   placeholder="Search . . . ." name="search"
                   value="{{ (isset($_GET['search']) && !empty($_GET['search']))?$_GET['search']:"" }}">
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
        We Have Total {{$count}}  Entries

         {{--<p>List of Previous Lotteries {{ $person->created_at}}</p>--}}

        {{--<div style="background:#00b38f; color: #ffffff; width: 600px;text-align: center; font-size: 20px;">{{ session('message') }}</div>--}}
    </div>
                    <div class="panel-body">
    <table id="example1" class="table table-responsive table table-bordered table-striped" style="text-align:center">
        <thead >
        <tr class="bg-primary" >
            <th style="text-align: center; ">SL#</th>
            {{--<th style="text-align:center">Unique Id</th>//don't show unique id here--}}
            <th style="text-align:center">Name</th>
            <th style="text-align:center">Phone</th>
            <th style="text-align:center">Gender</th>
            <th style="text-align:center">Action</th>

        </tr>
        </thead>
        <tbody>


        @foreach( $persons as $person )


            <tr>
                <td>{{ $loop->index + 1}}</td>
                <td>{{ $person->name }}</td>
                <td>{{ $person->phone}}</td>
                <td>{{ $person->gender}}</td>
                <td> <a href="{{ route('person.show',$person->id) }}" class=" btn btn-info" title="View"><span class=" glyphicon glyphicon-eye-open"></span></a>
                 {{--<a href="{{ route('lottery.view') }}" class=" btn btn-info"><span class=" glyphicon glyphicon-eye-open"></span></a>--}}

                    <a href="{{ route('person.edit',$person->id) }}" class=" btn btn-primary" title="Edit"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $person->id }}" method="POST" action="{{ route('person.destroy',$person->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $person->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-danger" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>

                </td>
            </tr>
        @endforeach


        </tbody>

    </table>
       </div>
    {{ $persons->links() }}
          </div>
      </div>
   </div>
</div>
@endsection