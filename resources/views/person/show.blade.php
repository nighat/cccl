

@extends('layouts.master')

@section('content')

                <div class="panel panel-success">

                    <div class="panel-heading ">
                        <h2 >Person Information
                            <a href="{{ route('person.create') }}" class=" btn btn-primary pull-right"> <span class=" glyphicon glyphicon-plus-sign"></span>Create</a>
                            <a href="{{ route('person.index') }}" class=" btn btn-primary pull-right">Run Person</a>
                        </h2>

                    </div>
                    <div class="panel-body">
                    <table style="text-align: center" id="example1" class="table table-bordered table-striped">
                        <thead >
                        <tr class="bg-primary">
                            <th style="text-align: center;font-size: 20px">Title</th>
                            <th style="text-align: center;font-size: 20px">Information</th>


                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Unique Id</td>
                            <td>{{ $person->unique_id }}</td>
                        </tr>
                            <tr>
                                <td>Name</td>
                                <td>{{ $person->name }}</td>
                            </tr>
                             <tr>
                                <td>Father Name</td>
                                <td>{{$person->father_name}}</td>
                            </tr>
                            <tr>
                                <td>Mother Name</td>
                                <td>{{$person->mother_name}}</td>
                            </tr>
                             <tr>
                                <td>Address</td>
                                <td>{{$person->address}}</td>
                            </tr>
                            <tr>
                                <td>Address1</td>
                                <td>{{$person->address1}}</td>
                            </tr>
                             <tr>
                                <td>Address2</td>
                                <td>{{$person->address2}}</td>
                            </tr>
                            <tr>
                                <td>Category</td>
                                <td>{{$person->category}}</td>
                            </tr>
                            <tr>
                                <td>Reference</td>
                                <td>{{$person->ref}}</td>
                            </tr>
                            <tr>
                                <td>Area</td>
                                <td>{{$person->area}}</td>
                            </tr>
                          <tr>

                                <td>Ward Name</td>
                                <td>{{$person->ward_name}}</td>
                            </tr>
                           <tr>
                                <td>Phone</td>
                                <td>{{$person->phone}}</td>
                            </tr>
                           <tr>
                                <td>E-mail</td>
                                <td>{{$person->email}}</td>
                            </tr>

                           <tr>
                                <td>NID</td>
                                <td>{{$person->nid}}</td>
                            </tr>
                          <tr>
                                <td>Win</td>
                                <td>{{$person->is_win}}</td>
                            </tr>




                            <tr>
                                <td>Gender</td>
                                <td>{{$person->gender}}</td>
                            </tr>


                        </tbody>

                    </table>

      </div>








                  </div>

@endsection