@extends('layouts.master')

@section('content')

    <div class="panel panel-success">

        <div class="panel-heading">
            <h2>Ward Create Form</h2>
        </div>

        <div class="panel-body " >
            <form class="form-horizontal" method="POST" action="{{ route('ward.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}


                <div class="form-group">
                    <label for="title" class="col-md-2 control-label">Title</label>

                    <div class="col-md-6">
                        <input id="title" type="text" class="form-control " name="title" >
                    </div>
                </div>

                <div class="form-group">
                    <label for="ward_name" class="col-md-2 control-label">Ward Name</label>

                    <div class="col-md-6">
                        <input id="ward_name" type="text" class="form-control " name="ward_name" >
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">

                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ route('ward.index') }}" class=" btn btn-primary">Ward</a>
                    </div>
                </div>
            </form>
        </div>

    </div>

@endsection