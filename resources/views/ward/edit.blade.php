
@extends('layouts.master')

@section('content')

                <div class="panel panel-success">
                <div class="panel-heading"><h2 >Ward Edit Form</h2></div>

                <div class="panel-body " >
                    <form class="form-horizontal" method="POST" action="{{ route('ward.update',$ward->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}


                        <div class="form-group">
                            <label for="title" class="col-md-2 control-label">Title</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control " name="title" value="{{ $ward->title }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ward_name" class="col-md-2 control-label">Ward Name</label>

                            <div class="col-md-6">
                                <input id="ward_name" type="text" class="form-control " name="ward_name" value="{{ $ward->ward_name }}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">

                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{ route('ward.index') }}" class=" btn btn-primary">ward</a>
                                <a href="{{ route('ward.create') }}" class=" btn btn-primary"> <span class=" glyphicon glyphicon-plus-sign"></span>Create</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


@endsection
