@extends('layouts.master')

@section('content')

                <div class="panel panel-success">
    <div class="panel-heading">
        <h2 >
            Wards List
            <a href="{{ route('ward.create') }}" class=" btn btn-primary pull-right"> <span class=" glyphicon glyphicon-plus-sign"></span>Add  ward </a>

        </h2>



         {{--<p>List of Previous Lotteries {{ $ward->created_at}}</p>--}}

        {{--<div style="background:#00b38f; color: #ffffff; width: 600px;text-align: center; font-size: 20px;">{{ session('message') }}</div>--}}
    </div>
                    <div class="panel-body">
    <table id="example1" class="table table-responsive table table-bordered table-striped" style="text-align:center">
        <thead >
        <tr class="bg-primary" >
            <th style="text-align: center; ">SL#</th>
            {{--<th style="text-align:center">Unique Id</th>//don't show unique id here--}}
            <th style="text-align:center">Title</th>
            <th style="text-align:center">Ward</th>
            <th style="text-align:center">Action</th>

        </tr>
        </thead>
        <tbody>


        @foreach( $wards as $ward )


            <tr>
                <td>{{ $loop->index + 1}}</td>
                <td>{{ $ward->title }}</td>
                <td>{{ $ward->ward_name}}</td>
                <td> <a href="{{ route('ward.show',$ward->id) }}" class=" btn btn-info" title="View"><span class=" glyphicon glyphicon-eye-open"></span></a>
                 {{--<a href="{{ route('lottery.view') }}" class=" btn btn-info"><span class=" glyphicon glyphicon-eye-open"></span></a>--}}

                    <a href="{{ route('ward.edit',$ward->id) }}" class=" btn btn-primary" title="Edit"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $ward->id }}" method="POST" action="{{ route('ward.destroy',$ward->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $ward->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-danger" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>

                </td>
            </tr>
        @endforeach


        </tbody>

    </table>
       </div>
    {{ $wards->links() }}
          </div>
      </div>
   </div>
</div>
@endsection