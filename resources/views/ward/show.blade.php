
@extends('layouts.master')

@section('content')

                <div class="panel panel-success">

                    <div class="panel-heading ">
                        <h2 >Ward Information
                            <a href="{{ route('ward.create') }}" class=" btn btn-primary pull-right"> <span class=" glyphicon glyphicon-plus-sign"></span>Create</a>
                            <a href="{{ route('ward.index') }}" class=" btn btn-primary pull-right">Run Ward</a>
                        </h2>

                    </div>
                    <div class="panel-body">
                    <table style="text-align: center" id="example1" class="table table-bordered table-striped">
                        <thead >
                        <tr class="bg-primary">
                            <th style="text-align: center;font-size: 20px">Title</th>
                            <th style="text-align: center;font-size: 20px">Ward</th>


                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                            <td>{{ $ward->title }}</td>
                            <td>{{ $ward->ward_name }}</td>
                        </tr>


                        </tbody>

                    </table>

                   </div>








                  </div>

@endsection