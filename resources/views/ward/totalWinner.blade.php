@extends('layouts.master')

@section('content')

    <div class="panel panel-success">
        <div class="panel-heading">
            <h2 style="text-align: center">Total Ward</h2>
        </div>

        <div class="panel-body">
            <form class="form-horizontal" method="POST" action="{{ route('ward.saveMaxWinner')}}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}

                <div class="form-group">
                <label for="ward_name" class="col-md-4 control-label">Ward Name</label><br/>


                <div class="col-md-8">

                    @foreach ($wards as $ward)

                        <label class="form-check-label">
                            <table>
                                <tr >
                                 <td>{{ $ward->ward_name }}-{{ $ward->id }}:- </td>
                                <td > <input type="number" name="ward_{{$ward->id}}" value="0"> </td>
                                </tr>
                            {{--{{ $ward->ward_name }}-{{ $ward->id }}:-  <input type="number" name="wards[]" value="1">  <br/><br/>--}}
                                </table>
                        </label>

                    @endforeach


                </div>
                </div>


                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary">Save</button>

                </div>
            </form>
        </div>
    </div>

@endsection

