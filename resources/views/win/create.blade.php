@extends('layouts.master')

@section('content')

<div class="panel panel-success">
    <div class="panel-heading">
        <h2 style="text-align: center">Win Lottery Form</h2>
        <ul class="bg-warning text-white">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ route('win.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title" class="col-md-3 control-label">Title</label>
                <div class="col-md-6">
                    <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}">
                </div>
            </div>

            {{--<div class="form-group">--}}
                {{--<label for="number_Of_winners" class="col-md-3 control-label">Number Of Winners</label>--}}
                {{--<div class="col-md-6">--}}
                    {{--<input id="number_Of_winners" type="number" class="form-control" name="number_Of_winners" >--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="form-group">
                <label for="ward_name" class="col-md-3 control-label">Distribution Of Winners</label>
                <div class="col-md-6">
                    <table class="table">
                        <thead>
                            <th>Ward</th>
                            <th>Number Of Winners</th>
                        </thead>
                        <tbody>
                            @foreach ($wards as $key => $value)
                                <tr>
                                    <td><input type="hidden" name="ward_ids[]" value="{{ $key }}"> {{ $value }}</td>
                                    <td><input type="number" name="winners_per_word[]" placeholder="Number Of Winners" value="0"></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="form-group">
                <label for="differ_in_gender" class="col-md-3 control-label">Difference In Gender ? </label>

                <div class="col-md-6">

                        <label class="form-check-label">
                            <input type="checkbox" name="differ_in_gender" value="yes">

                        </label>


                </div>
            </div>


            {{--<div class="form-group">--}}
                {{--<label for="win_id" class="col-md-3 control-label">WinIds</label>--}}

                {{--<div class="col-md-6">--}}
                    {{--<textarea id="win_id" type="text" class="form-control" name="win_ids" readonly="readonly"></textarea>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="form-group text-center">

                    {{--<button type="button" onclick="getWinners()" class=" btn btn-warning">Get Winner</button>--}}
                    <button type="submit" class="btn btn-primary">Save</button>

            </div>
        </form>
    </div>
</div>

@endsection

{{--@push('scripts')--}}

{{--<script>--}}

      {{--function shuffle(array) {--}}
          {{--var currentIndex = array.length, temporaryValue, randomIndex;--}}
          {{--// While there remain elements to shuffle...--}}
          {{--while (0 !== currentIndex) {--}}
              {{--// Pick a remaining element...--}}
              {{--randomIndex = Math.floor(Math.random() * currentIndex);--}}
              {{--currentIndex -= 1;--}}
              {{--// And swap it with the current element.--}}
              {{--temporaryValue = array[currentIndex];--}}
              {{--array[currentIndex] = array[randomIndex];--}}
              {{--array[randomIndex] = temporaryValue;--}}
          {{--}--}}
          {{--return array;--}}
      {{--}--}}

      {{--function getWinners(){--}}
          {{--var winIds = shuffle({{$win_ids}}).slice(0,100);--}}
          {{--$('#win_id').val(winIds);--}}
      {{--}--}}




{{--</script>--}}


{{--@endpush--}}