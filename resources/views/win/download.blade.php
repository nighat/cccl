

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="panel panel-success">

    <div class="panel-heading ">
        <h1 style="text-align: center;color: blue">
            Winners of Lottery
        </h1>

    </div>

    <div class="panel-body">
        {{--<h2 class="text-center">{{ $win->title}}</h2>--}}

        <table class="table table-bordered table-striped"  style="width:100%">

            <thead style="border: 1px solid black;text-align: center">

            <tr class="bg-info">
                <th>Name</th>
                <th>Phone</th>
                <th>Address</th>
            </tr>
            </thead>
            {{--{{ $win->win_ids }}--}}
            <tbody >


            @foreach( $persons as $person )

                <tr>

                    <td>{{ $person->name}}</td>
                    <td>{{ $person->phone}}</td>
                    <td>{{ $person->address }}</td>

                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>
</body>
</html>
