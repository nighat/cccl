@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 style="text-align: center;color: darkblue">Winner Edit Form</h3></div>


                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('win.update',$win->id) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" value="{{ $win->title}}">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="win_ids" class="col-md-4 control-label">Win Ids</label>

                                <div class="col-md-6">
                                    <input id="win_ids" type="text" class="form-control" name="win_ids" value="{{ $win->win_ids}}" >
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">

                                    <button type="submit" class="btn btn-primary">Submit</button>

                                    <a href="{{ route('win.create') }}" class=" btn btn-primary">Create</a>
                                    <a href="{{ route('win.index') }}" class=" btn btn-primary">win</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection