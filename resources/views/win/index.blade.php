@extends('layouts.master')

@section('content')

 <div class="panel panel-success">

    <div class="panel-heading">
        <h2 style="text-align: center">
            Lists of All Lotteries
            <a href="{{ route('win.create') }}" class=" btn btn-primary pull-right"> <span class=" glyphicon glyphicon-plus-sign"></span>New Lottery Event </a>
        </h2>
        {{--<form class="form-inline" action="{{ route('win.index') }}" method="get">--}}
            {{--<input type="search" class="form-control colorize-theme6-bg" id="search"--}}
                   {{--placeholder="Search . . . ." name="search"--}}
                   {{--value="{{ (isset($_GET['search']) && !empty($_GET['search']))?$_GET['search']:"" }}">--}}
            {{--<button type="submit" class="btn btn-primary">Search</button>--}}
        {{--</form>--}}
        {{--//We Have Total {{$count}} Entries--}}
        <div style="background:#00b38f; color: #ffffff; width: 600px;text-align: center; font-size: 20px;">
            {{ session('message') }}
        </div>
    </div>
     <div class="panel-body">
        <table id="example1" class="table table-responsive table-bordered table-striped">
            <thead >
                <tr class="bg-primary">
                    <th width="100">SL#</th>
                    <th class="text-left">Title</th>
                    <th class="text-right">Action</th>

                </tr>
            </thead>
            <tbody>
                @foreach($wins as $win)

                    <tr>
                        <td>{{ $loop->index + 1}}</td>
                        <td>{{ $win->title}}</td>
                        <td  class="text-right" width="150">
                            <a href="{{ route('win.show',$win->id) }}" class=" btn btn-info" title="View Winners"><span class="glyphicon glyphicon-eye-open"></span></a>
                            <a href="{{ url('winpdf/'.$win->id)}}" class=" btn btn-primary" title="Download PDF "><span class="glyphicon glyphicon-download"></span></a>
                            {{--<a href="#" class=" btn btn-danger" title="Send Mail"><span class="glyphicon glyphicon-envelope"></span></a>--}}
                        </td>
                    </tr>
                @endforeach
            </tbody>

        </table>

        <div class="pull-right">{{ $wins->links() }}</div>
     </div>
</div>
@endsection