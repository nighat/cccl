@extends('layouts.master')

@section('content')

<div class="panel panel-success">

   <div class="panel-heading ">
         <h2>Winners of {{ $win->title }}
         <a href="{{ route('winners.download_excel', $win->id) }}" class=" btn btn-info pull-right">Download Excel</a>&nbsp;
         <a href="{{ route('winners.send_email', $win->id) }}" class=" btn btn-danger pull-right">Send Email</a>&nbsp;
         <a href="{{ route('win.create') }}" class=" btn btn-primary pull-right">Run New Lottery</a>&nbsp;
         <a href="{{ route('win.index') }}" class=" btn btn-primary pull-right">List</a>&nbsp;
       </h2>
       <div style="background:#00b38f; color: #ffffff; width: 600px;text-align: center; font-size: 20px;">
           {{ session('message') }}
       </div>
   </div>
    <div class="panel-body">
     <table class="table table-bordered table-striped">
         <thead >
         <tr class="bg-info">
             <th width="100">SL#</th>
             <th>Name</th>
             <th>Gender</th>
             <th>Ward</th>
             <th>Phone</th>
             <th>Address</th>
         </tr>
         </thead>
         <tbody>
            @foreach( $persons  as $person )
             <tr>
                 <td>{{ $loop->index + 1}}</td>
                 <td>{{ $person->name }}</td>
                 <td>{{ $person->gender }}</td>
                 <td>{{ $person->ward->ward_name }}</td>
                 <td>{{ $person->phone }}</td>
                 <td>{{ $person->address }}</td>
             </tr>
            @endforeach
         </tbody>
     </table>
    </div>
</div>
@endsection