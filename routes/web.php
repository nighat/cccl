<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware'=>'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/totalWinner', 'WardController@totalWinner');
    Route::resource('people', 'PersonController');//resource route
    Route::resource('person', 'PersonController');//resource route
    Route::resource('win', 'WinController');//resource route
    Route::get('winpdf/{win_ids}', 'WinController@download');
    Route::resource('ward', 'WardController');
//    Route::post('ward', 'WardController@saveMaxWinner');

    Route::get('winners/{win}/download_excel', 'WinController@downloadExcel')->name('winners.download_excel');
    Route::get('winners/{win}/send_email', 'WinController@sendEmailToWinners')->name('winners.send_email');

});